package com.calt.plm.basic.core.featuretest.dto;

public class DocumentDTO {
    /**
     * 主键
     */
    private String id;
    /**
     * 文档名称
     */
    private String documentName;
    /**
     * 文档编号
     */
    private String documentCode;
    /**
     * 实例唯一标识
     */
    private String bizId;
    /**
     * 生命周期策略主键
     */
    private String policyId;
    /**
     * 生命周期状态
     */
    private String bizCurrent;
    /**
     * -1：删除；1-当前；0-历史
     */
    private String avariable;
    /**
     * 生命周期版本
     */
    private String bizVersion;
}
