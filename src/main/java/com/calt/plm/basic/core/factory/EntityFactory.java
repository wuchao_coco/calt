package com.calt.plm.basic.core.factory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.apache.commons.lang3.ClassUtils;

import com.calt.plm.basic.core.model.EntityDescribe;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class EntityFactory
{
	public static final Map<String, EntityDescribe> CACHE = new ConcurrentHashMap();

	public static void registerEntity(Class<?> entity, String name, DataSource source)
	{
		String className = ClassUtils.getShortClassName(entity);
		EntityDescribe describe = CACHE.get(className);
		if(null == describe)
		{
			EntityDescribe load = new EntityDescribe(entity, name, source);
			CACHE.put(className, load);
		}
	}

	public static EntityDescribe getDescribe(Class<?> entityClazz)
	{
		if(null == entityClazz)
		{
			return null;
		}
		String className = ClassUtils.getShortClassName(entityClazz);
		return CACHE.get(className);
	}

}
