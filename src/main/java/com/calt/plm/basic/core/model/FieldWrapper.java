package com.calt.plm.basic.core.model;

import java.lang.reflect.Field;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class FieldWrapper
{
	private String columnName;

	private Field field;

	private boolean isActionField;

	public FieldWrapper(String columnName, Field field)
	{
		this.columnName = columnName;
		this.field = field;
		isActionField = false;
	}

	public FieldWrapper(String columnName)
	{
		this.columnName = columnName;
		isActionField = true;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public String getFieldName()
	{
		if(isActionField)
		{
			return this.columnName;
		}
		return this.field.getName();
	}

	public boolean isActionField()
	{
		return isActionField;
	}

	public String getSqlPart()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(this.columnName).append(" =:").append(getFieldName());
		return sb.toString();
	}
}
