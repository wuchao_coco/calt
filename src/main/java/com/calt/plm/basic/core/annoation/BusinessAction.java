package com.calt.plm.basic.core.annoation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>业务动作注解</p>
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BusinessAction
{
	/**
	 * 动作名称
	 * @return 动作名称
	 */
	String name();
}
