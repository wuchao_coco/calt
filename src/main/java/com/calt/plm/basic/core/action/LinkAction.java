package com.calt.plm.basic.core.action;

import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.calt.plm.basic.core.entity.GLLink;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>业务动作</p>
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
public interface LinkAction extends Action {
    default void linkSave(EntityContext context) {
        String roleAObjectId = (String) context.getVariable("roleAObjectId");
        List<String> roleBObjectIdList = (List<String>) context.getVariable("roleBObjectId");
        List<AbstractActionEntity> linkEntityList = context.getEntity(this.getClass());
        linkEntityList.forEach(linkEntity -> {
            List<GLLink> glLinkList = new ArrayList<>(roleBObjectIdList.size());
            roleBObjectIdList.forEach(roleBObjectId -> {
                GLLink glLink = (GLLink) linkEntity;
                glLink.setRoleAId(roleAObjectId);
                glLink.setRoleBId(roleBObjectId);
                glLinkList.add(glLink);
            });
            this.save(context, glLinkList);
        });
    }
}
