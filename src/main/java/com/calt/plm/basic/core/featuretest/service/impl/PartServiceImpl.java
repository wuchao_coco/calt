package com.calt.plm.basic.core.featuretest.service.impl;

import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.calt.plm.basic.core.factory.EntityContextFactory;
import com.calt.plm.basic.core.featuretest.dto.PartDTO;
import com.calt.plm.basic.core.featuretest.entity.DocumentEntity;
import com.calt.plm.basic.core.featuretest.entity.PartDocLinkEntity;
import com.calt.plm.basic.core.featuretest.entity.PartEntity;
import com.calt.plm.basic.core.featuretest.service.PartService;
import com.calt.plm.basic.core.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PartServiceImpl implements PartService
{
    @Autowired
    private EntityService entityService;

    @Override
    public void test(PartDTO partDTO) {
        PartDocLinkEntity linkEntity = new PartDocLinkEntity(null);
        linkEntity.setTitle("link关系保存测试");
        linkEntity.setRemark("ttttttttttaaaaaaaaaaaaaa");
        linkEntity.setRoleAId("123");
        linkEntity.setRoleAClass("Part");
        linkEntity.setRoleBId("456");
        linkEntity.setRoleBClass("Doc");
        EntityContext context = EntityContextFactory.getContext(linkEntity);
        PartDocLinkEntity entityAction = ((PartDocLinkEntity) context.getEntityHandle(PartDocLinkEntity.class));
        entityAction.save(context, context.getEntity(PartDocLinkEntity.class));
        entityService.finish(context);
    }

    @Override
    public void batchTest(PartDTO partDTO) {
        List<DocumentEntity> documentEntityList = new ArrayList<>(3);
        DocumentEntity documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("天书奇谭");
        documentEntity.setDocumentCode("A001");
        documentEntityList.add(documentEntity);
        documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("阿猫阿狗");
        documentEntity.setDocumentCode("A002");
        documentEntityList.add(documentEntity);
        documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("小城故事不多");
        documentEntity.setDocumentCode("A003");
        documentEntityList.add(documentEntity);

        EntityContext context = EntityContextFactory.getContext(documentEntityList);
        DocumentEntity entityAction = (DocumentEntity) context.getEntityHandle(DocumentEntity.class);
        entityAction.lifeCycleInit(context);
        entityService.finish(context);
    }

    @Override
    public void bizRel(PartDTO partDTO) {
        List<DocumentEntity> documentEntityList = new ArrayList<>(3);
        DocumentEntity documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("###天书奇谭");
        documentEntity.setDocumentCode("###A001");
        documentEntityList.add(documentEntity);
        documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("###阿猫阿狗");
        documentEntity.setDocumentCode("###A002");
        documentEntityList.add(documentEntity);
        documentEntity = new DocumentEntity(null);
        documentEntity.setDocumentName("###小城故事不多");
        documentEntity.setDocumentCode("###A003");
        documentEntityList.add(documentEntity);

        PartEntity partEntity = getEntityByDTO(partDTO);

        List<PartDocLinkEntity> linkEntityList = new ArrayList<>(documentEntityList.size());
        PartDocLinkEntity linkEntity = new PartDocLinkEntity(null);
        linkEntity.setTitle("link关系保存测试111");
        linkEntity.setRemark("111ttttttttttaaaaaaaaaaaaaa");
//        linkEntity.setRoleAId("123");//?
        linkEntity.setRoleAClass(PartDocLinkEntity.class.getName());
//        linkEntity.setRoleBId("456");//?
        linkEntity.setRoleBClass(DocumentEntity.class.getName());
        linkEntityList.add(linkEntity);
        linkEntity = new PartDocLinkEntity(null);
        linkEntity.setTitle("link关系保存测试222");
        linkEntity.setRemark("222ttttttttttaaaaaaaaaaaaaa");
//        linkEntity.setRoleAId("123");//?
        linkEntity.setRoleAClass(PartDocLinkEntity.class.getName());
//        linkEntity.setRoleBId("456");//?
        linkEntity.setRoleBClass(DocumentEntity.class.getName());
        linkEntityList.add(linkEntity);
        linkEntity = new PartDocLinkEntity(null);
        linkEntity.setTitle("link关系保存测试333");
        linkEntity.setRemark("333ttttttttttaaaaaaaaaaaaaa");
//        linkEntity.setRoleAId("123");//?
        linkEntity.setRoleAClass(PartDocLinkEntity.class.getName());
//        linkEntity.setRoleBId("456");//?
        linkEntity.setRoleBClass(DocumentEntity.class.getName());
        linkEntityList.add(linkEntity);

        List<AbstractActionEntity> entityList = new ArrayList<>();
        entityList.addAll(documentEntityList);
        entityList.add(partEntity);
        entityList.addAll(linkEntityList);
        EntityContext context = EntityContextFactory.getContext(entityList);

        DocumentEntity entityActionTemplate = (DocumentEntity) context.getEntityHandle(DocumentEntity.class);
        context.setVariable("linkRelation", "B");
        entityActionTemplate.lifeCycleInit(context);
        PartEntity partActionTemplate = (PartEntity) context.getEntityHandle(PartEntity.class);
        context.setVariable("linkRelation", "A");
        partActionTemplate.lifeCycleInit(context);
        partActionTemplate.containerAdd(context);
        PartDocLinkEntity linkEntityActionTemplate = (PartDocLinkEntity) context.getEntityHandle(PartDocLinkEntity.class);
        linkEntityActionTemplate.linkSave(context);

        entityService.finish(context);
    }

    protected PartEntity getEntityByDTO(PartDTO partDTO)
    {
        PartEntity partEntity = new PartEntity(null);
        partEntity.setId(partDTO.getId());
        partEntity.setCreateBy(partDTO.getCreateBy());
        partEntity.setUpdateBy(partDTO.getUpdateBy());
        partEntity.setFolderId(partDTO.getFolderId());
        partEntity.setPartCode(partDTO.getPartCode());
        partEntity.setPartPrice(partDTO.getPartPrice());
        partEntity.setProductionTime(partDTO.getProductionTime());
        partEntity.putActionProp("containerId", partDTO.getContainerId());
        return partEntity;
    }
}
