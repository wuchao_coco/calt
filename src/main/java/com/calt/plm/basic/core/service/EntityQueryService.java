package com.calt.plm.basic.core.service;

import com.calt.plm.basic.core.entity.AbstractEntity;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public interface EntityQueryService {

    AbstractEntity get(String entityName,String id);

}
