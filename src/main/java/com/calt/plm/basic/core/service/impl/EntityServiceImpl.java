package com.calt.plm.basic.core.service.impl;

import com.calt.plm.basic.core.command.EntityCommand;
import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.calt.plm.basic.core.factory.EntityCommandFactory;
import com.calt.plm.basic.core.service.EntityService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EntityServiceImpl implements EntityService
{
    @Override
    public void finish(EntityContext context)
    {
        List insertEntityList = context.getInsertEntity();
        insertEntityList.forEach(insertEntity -> {
            EntityCommand entityCommand = EntityCommandFactory.get(insertEntity.getClass());
            entityCommand.insert((AbstractActionEntity) insertEntity);
        });

        List modifyEntityList = context.getModifyEntity();
        modifyEntityList.forEach(updateEntity -> {
            EntityCommand entityCommand = EntityCommandFactory.get(updateEntity.getClass());
            entityCommand.modify((AbstractActionEntity) updateEntity);
        });

        List deleteEntityList = context.getDeleteEntity();
        deleteEntityList.forEach(deleteEntity -> {
            EntityCommand entityCommand = EntityCommandFactory.get(deleteEntity.getClass());
            entityCommand.delete(((AbstractActionEntity) deleteEntity).getId());
        });
    }
}
