package com.calt.plm.basic.core.factory;

import com.calt.plm.basic.core.command.EntityCommand;
import com.calt.plm.basic.core.command.kernel.EntityCommandImpl;
import com.calt.plm.basic.core.command.kernel.EntityExecuter;
import com.calt.plm.basic.core.model.EntityDescribe;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class EntityCommandFactory
{

	public static EntityCommand get(Class<?> entityClazz)
	{
		EntityDescribe describe = EntityFactory.getDescribe(entityClazz);
		if(null == describe)
		{
			throw new RuntimeException();
		}
		return new EntityCommandImpl(new EntityExecuter(describe));
	}

}
