package com.calt.plm.basic.core.context;

import java.util.List;

import com.calt.plm.basic.core.entity.AbstractActionEntity;

/**
 * <p>实体上下文</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public interface EntityContext extends ExternalParamContext, BeanContext, EntityResultContext
{
	void setEntity(List<? extends AbstractActionEntity> entities);

	List getEntity(Class clazz);

	void setVariable(String name, Object value);

	Object getVariable(String name);

	AbstractActionEntity getEntityHandle(Class clazz);

	void setToken(String token);

	String getToken();

}
