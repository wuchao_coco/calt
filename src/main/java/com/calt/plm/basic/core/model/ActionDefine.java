package com.calt.plm.basic.core.model;

import java.io.Serializable;
import java.util.List;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class ActionDefine implements Serializable {

    private String name;

    private List<ActionProp> propList;

    public ActionDefine() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ActionProp> getPropList() {
        return propList;
    }

    public void setPropList(List<ActionProp> propList) {
        this.propList = propList;
    }
}
