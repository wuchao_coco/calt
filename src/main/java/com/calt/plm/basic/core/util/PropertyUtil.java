package com.calt.plm.basic.core.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class PropertyUtil
{

	public static Object getPropValue(Object object, String propName)
	{
		try
		{
			return PropertyUtils.getProperty(object, propName);
		}
		catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
		{
			return null;
		}
	}

}
