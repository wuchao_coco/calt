package com.calt.plm.basic.core.container;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class SpringBeanContainer implements ApplicationContextAware
{

	public static ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		SpringBeanContainer.applicationContext = applicationContext;
	}

	/**
	 * 获取bean
	 * @param name 名称
	 * @return bean
	 */
	public static Object getBean(String name)
	{
		return applicationContext.getBean(name);
	}

	/**
	 * 获取bean
	 * @param clazz 类
	 * @return bean
	 */
	public static <T> T getBean(Class<T> clazz)
	{
		return applicationContext.getBean(clazz);
	}

	/**
	 * 获取所有类型的bean
	 * @param type 类
	 * @return 所有的bean
	 */
	public static <T> Map<String, T> getBeansOfType(Class<T> type)
	{
		return applicationContext.getBeansOfType(type);
	}
}
