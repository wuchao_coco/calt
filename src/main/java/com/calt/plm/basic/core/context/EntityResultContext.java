package com.calt.plm.basic.core.context;

import com.calt.plm.basic.core.entity.AbstractActionEntity;

import java.util.List;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public interface EntityResultContext
{
	void addInsertEntity(AbstractActionEntity entity);

	List getInsertEntity();

	void addModifyEntity(AbstractActionEntity entity);

    List getModifyEntity();

	void addDeleteEntity(AbstractActionEntity entity);

    List getDeleteEntity();

}
