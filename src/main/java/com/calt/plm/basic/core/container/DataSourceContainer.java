package com.calt.plm.basic.core.container;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * <p>数据源容器</p>
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class DataSourceContainer implements ApplicationContextAware
{
	public static Map<String, DataSource> dataSourceMap;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		DataSourceContainer.dataSourceMap = applicationContext.getBeansOfType(DataSource.class);
	}

	/**
	 * 根据数据源名称获取数据源
	 * @param name 数据源名称
	 * @return 数据源
	 */
	public static DataSource get(String name)
	{
		return dataSourceMap.get(name);
	}

}
