package com.calt.plm.basic.core.featuretest.entity;

import com.calt.plm.basic.core.action.LifeCycleI;
import com.calt.plm.basic.core.annoation.BusinessAction;
import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;
import com.calt.plm.basic.core.entity.GLObject;

@BusinessAction(name = "plm_document")
public class DocumentEntity extends GLObject implements LifeCycleI
{
	@Prop(name = "document_name", type = DataType.VARCHAR, length = 200)
	private String documentName;
	@Prop(name = "document_code", type = DataType.VARCHAR, length = 30, index = true)
	private String documentCode;

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	public String getDocumentCode()
	{
		return documentCode;
	}

	public void setDocumentCode(String documentCode)
	{
		this.documentCode = documentCode;
	}

}
