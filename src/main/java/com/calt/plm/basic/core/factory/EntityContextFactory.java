package com.calt.plm.basic.core.factory;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.context.kernel.EntityContextImpl;
import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.google.common.collect.Lists;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class EntityContextFactory
{

	public static EntityContext getContext(AbstractActionEntity entity)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(Lists.newArrayList(entity));
		return context;
	}

	public static EntityContext getContext(AbstractActionEntity entity, String token)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(Lists.newArrayList(entity));
		context.setToken(token);
		return context;
	}

	public static EntityContext getContext(AbstractActionEntity entity, Map<String, Object> params)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(Lists.newArrayList(entity));
		if(MapUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> entry : params.entrySet())
			{
				context.setParam(entry.getKey(), entry.getValue());
			}
		}
		return context;
	}

	public static EntityContext getContext(AbstractActionEntity entity, Map<String, Object> params, String token)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(Lists.newArrayList(entity));
		if(MapUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> entry : params.entrySet())
			{
				context.setParam(entry.getKey(), entry.getValue());
			}
		}
		context.setToken(token);
		return context;
	}

	public static EntityContext getContext(List<? extends AbstractActionEntity> entities)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(entities);
		return context;
	}

	public static EntityContext getContext(List<? extends AbstractActionEntity> entities, String token)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(entities);
		context.setToken(token);
		return context;
	}

	public static EntityContext getContext(List<AbstractActionEntity> entities, Map<String, Object> params)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(entities);
		if(MapUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> entry : params.entrySet())
			{
				context.setParam(entry.getKey(), entry.getValue());
			}
		}
		return context;
	}

	public static EntityContext getContext(List<AbstractActionEntity> entities, Map<String, Object> params,
			String token)
	{
		EntityContextImpl context = new EntityContextImpl();
		context.setEntity(entities);
		if(MapUtils.isNotEmpty(params))
		{
			for (Map.Entry<String, Object> entry : params.entrySet())
			{
				context.setParam(entry.getKey(), entry.getValue());
			}
		}
		context.setToken(token);
		return context;
	}

}
