package com.calt.plm.basic.core.context.kernel;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

import com.calt.plm.basic.core.container.SpringBeanContainer;
import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.calt.plm.basic.core.entity.Entity;
import com.calt.plm.basic.core.util.ClassUtil;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class EntityContextImpl extends ExternalParamPack implements EntityContext
{
	private Map<String, Object> variables;

	private Multimap<String, AbstractActionEntity> resultMaps;

	private Multimap<String, AbstractActionEntity> entities;

	private String token;

	public EntityContextImpl()
	{
		super();
		this.variables = new HashMap<>();
		this.entities = ArrayListMultimap.create();
		this.resultMaps = ArrayListMultimap.create();
	}

	@Override
	public void setEntity(List<? extends AbstractActionEntity> entities)
	{

		if(CollectionUtils.isEmpty(entities))
		{
			return;
		}
		for (AbstractActionEntity entity : entities)
		{
			this.entities.put(ClassUtil.getShortClassName(entity.getClass()), entity);
		}
	}

	@Override
	public List<? extends AbstractActionEntity> getEntity(Class clazz)
	{
		return Lists.newArrayList(this.entities.get(ClassUtil.getShortClassName(clazz.getName())).iterator());
	}

	@Override
	public AbstractActionEntity getEntityHandle(Class clazz)
	{
		Collection<AbstractActionEntity> entities = this.entities.get(ClassUtil.getShortClassName(clazz.getName()));
		if(CollectionUtils.isEmpty(entities))
		{
			return null;
		}
		return CollectionUtils.get(entities, 0);
	}

	@Override
	public void setToken(String token)
	{
		this.token = token;
	}

	@Override
	public String getToken()
	{
		return this.token;
	}

	@Override
	public void setVariable(String name, Object value)
	{
		this.variables.put(name, value);
	}

	@Override
	public Object getVariable(String name)
	{
		return this.variables.get(name);
	}

	@Override
	public Object getBean(String name)
	{
		return SpringBeanContainer.getBean(name);
	}

	@Override
	public <T> T getBean(Class<T> clazz)
	{
		return SpringBeanContainer.getBean(clazz);
	}

	@Override
	public <T> Map<String, T> getBeansOfType(Class<T> type)
	{
		return SpringBeanContainer.getBeansOfType(type);
	}

	@Override
	public void addInsertEntity(AbstractActionEntity entity)
	{
		this.resultMaps.put(Entity.OPER_TYPE_INSERT, entity);
	}

	@Override
	public List getInsertEntity()
	{
		return Lists.newArrayList(this.resultMaps.get(Entity.OPER_TYPE_INSERT).iterator());
	}

	@Override
	public void addModifyEntity(AbstractActionEntity entity)
	{
		this.resultMaps.put(Entity.OPER_TYPE_MODIFY, entity);
	}

	@Override
	public List getModifyEntity()
	{
		return Lists.newArrayList(this.resultMaps.get(Entity.OPER_TYPE_MODIFY).iterator());
	}

	@Override
	public void addDeleteEntity(AbstractActionEntity entity)
	{
		this.resultMaps.put(Entity.OPER_TYPE_DELETE, entity);
	}

	@Override
	public List getDeleteEntity()
	{
		return Lists.newArrayList(this.resultMaps.get(Entity.OPER_TYPE_DELETE).iterator());
	}

}
