package com.calt.plm.basic.core.entity;

/**
 * <p>实体</p>
 *
 * @author glaway Create on 2022/9/22
 * @version 1.0
 */
public abstract class Entity
{
	/**
	 * 操作类型-写入
	 */
	public static final String OPER_TYPE_INSERT = "I";

	/**
	 * 操作类型-修改
	 */
	public static final String OPER_TYPE_MODIFY = "M";

	/**
	 * 操作类型-删除
	 */
	public static final String OPER_TYPE_DELETE = "D";

}
