package com.calt.plm.basic.core.context.kernel;

import java.util.HashMap;
import java.util.Map;

import com.calt.plm.basic.core.context.ExternalParamContext;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class ExternalParamPack implements ExternalParamContext
{

	private Map<String, Object> params;

	public ExternalParamPack()
	{
		this.params = new HashMap<>();
	}

	@Override
	public void setParam(String name, Object value)
	{
		params.put(name, value);
	}

	@Override
	public Object getParam(String name)
	{
		return params.get(name);
	}

	@Override
	public Object getParam(String name, Object defaultValue)
	{
		return params.getOrDefault(name, defaultValue);
	}
}
