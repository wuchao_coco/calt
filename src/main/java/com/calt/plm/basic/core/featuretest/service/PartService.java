package com.calt.plm.basic.core.featuretest.service;

import com.calt.plm.basic.core.featuretest.dto.PartDTO;

public interface PartService
{
//    PartDTO addPart(PartDTO partDTO);

    void test(PartDTO partDTO);

    void batchTest(PartDTO partDTO);

    void bizRel(PartDTO partDTO);
}
