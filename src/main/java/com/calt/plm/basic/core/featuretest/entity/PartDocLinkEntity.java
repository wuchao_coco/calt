package com.calt.plm.basic.core.featuretest.entity;

import com.calt.plm.basic.core.action.LinkAction;
import com.calt.plm.basic.core.annoation.BusinessAction;
import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;
import com.calt.plm.basic.core.entity.GLLink;

@BusinessAction(name = "plm_part_doc_link")
public class PartDocLinkEntity extends GLLink implements LinkAction
{
	@Prop(name = "title", type = DataType.VARCHAR, length = 200)
	private String title;
	@Prop(name = "remark", type = DataType.VARCHAR, length = 2000)
	private String remark;

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

}
