package com.calt.plm.basic.core.entity;

import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;

public abstract class GLObject extends AbstractActionEntity
{
	@Prop(name = "create_by", type = DataType.VARCHAR, length = 32, index = true)
	private String createBy;
	@Prop(name = "update_by", type = DataType.VARCHAR, length = 32, index = true)
	private String updateBy;

	public String getCreateBy()
	{
		return createBy;
	}

	public void setCreateBy(String createBy)
	{
		this.createBy = createBy;
	}

	public String getUpdateBy()
	{
		return updateBy;
	}

	public void setUpdateBy(String updateBy)
	{
		this.updateBy = updateBy;
	}

}
