package com.calt.plm.basic.core.service;

import com.calt.plm.basic.core.context.EntityContext;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public interface EntityService {


    void finish(EntityContext context);

}
