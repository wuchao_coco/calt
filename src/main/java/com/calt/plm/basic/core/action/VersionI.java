package com.calt.plm.basic.core.action;

import com.calt.plm.basic.core.annoation.BusinessAction;
import com.calt.plm.basic.core.context.EntityContext;

@BusinessAction(name = "version")
public interface VersionI extends Action {

    default void versionRevise(EntityContext context)
    {
        //
    }
}
