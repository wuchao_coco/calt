package com.calt.plm.basic.core.featuretest.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.calt.plm.basic.core.action.ContainerI;
import com.calt.plm.basic.core.action.LifeCycleI;
import com.calt.plm.basic.core.annoation.BusinessEntity;
import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;
import com.calt.plm.basic.core.entity.GLObject;

@BusinessEntity(name = "plm_part")
public class PartEntity extends GLObject implements LifeCycleI, ContainerI
{
	@Prop(name = "folder_id", type = DataType.VARCHAR, length = 32, index = true)
	private String folderId;
	@Prop(name = "part_code", type = DataType.VARCHAR, index = true)
	private String partCode;
	@Prop(name = "part_price", type = DataType.NUMBER, length = 20, precision = 2)
	private BigDecimal partPrice;
	@Prop(name = "production_time", type = DataType.DATETIME)
	private Date productionTime;

	public String getFolderId()
	{
		return folderId;
	}

	public void setFolderId(String folderId)
	{
		this.folderId = folderId;
	}

	public String getPartCode()
	{
		return partCode;
	}

	public void setPartCode(String partCode)
	{
		this.partCode = partCode;
	}

	public BigDecimal getPartPrice()
	{
		return partPrice;
	}

	public void setPartPrice(BigDecimal partPrice)
	{
		this.partPrice = partPrice;
	}

	public Date getProductionTime()
	{
		return productionTime;
	}

	public void setProductionTime(Date productionTime)
	{
		this.productionTime = productionTime;
	}

	public PartEntity()
	{
	}

}
