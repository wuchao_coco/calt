package com.calt.plm.basic.core.model;

import java.io.Serializable;

import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.entity.AbstractEntity;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class ActionProp implements Serializable
{
	private static final long serialVersionUID = -4142535188484512911L;

	private String name;

	private DataType type;

	private String desc;

	private Integer length;

	private Integer precision;

	private Boolean index;

	public ActionProp()
	{
	}

	public ActionProp(String name, DataType type, String desc, Integer length, Integer precision, Boolean index)
	{
		this();
		this.name = name;
		this.type = type;
		this.desc = desc;
		this.length = length;
		this.precision = precision;
		this.index = index;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public DataType getType()
	{
		return type;
	}

	public void setType(DataType type)
	{
		this.type = type;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public Integer getLength()
	{
		return length;
	}

	public void setLength(Integer length)
	{
		this.length = length;
	}

	public Integer getPrecision()
	{
		return precision;
	}

	public void setPrecision(Integer precision)
	{
		this.precision = precision;
	}

	public Boolean getIndex()
	{
		return index;
	}

	public void setIndex(Boolean index)
	{
		this.index = index;
	}

	public String buildProp(ActionProp actionProp)
	{
		StringBuffer propBuffer = new StringBuffer();
		propBuffer.append(actionProp.getName().toUpperCase()).append(" ");
		if(DataType.DATETIME.equals(actionProp.getType()))
		{
			propBuffer.append("DATE").append(" ");
		}
		else if(DataType.NUMBER.equals(actionProp.getType()))
		{
			propBuffer.append("NUMBER").append("(").append(actionProp.getLength());
			if(null != actionProp.getPrecision() && actionProp.getPrecision() > 0)
			{
				propBuffer.append(",").append(actionProp.getPrecision());
			}
			propBuffer.append(")").append(" ");
		}
		else
		{
			if(null != actionProp.getLength() && -1 == actionProp.getLength())
			{
				propBuffer.append("CLOB").append(" ");
			}
			else
			{
				propBuffer.append("VARCHAR2").append("(").append(actionProp.getLength()).append(")").append(" ");
			}
		}

		if(AbstractEntity.COLUMN_ID.equalsIgnoreCase(actionProp.getName()))
		{
			propBuffer.append(" NOT NULL PRIMARY KEY").append(",");
		}
		else
		{
			propBuffer.append(",");
		}
		return propBuffer.toString();
	}

	public String buildIndex(String tableName, ActionProp actionProp)
	{
		StringBuffer indexBuffer = new StringBuffer();
		if(actionProp.getIndex() && !AbstractEntity.COLUMN_ID.equalsIgnoreCase(actionProp.getName()))
		{
			indexBuffer.append("CREATE INDEX ").append("IDX").append("_").append(tableName.toUpperCase()).append("_")
					.append(actionProp.getName().toUpperCase()).append(" ON ").append(tableName).append("(")
					.append(actionProp.getName().toUpperCase()).append(")");
		}
		return indexBuffer.toString();
	}
}
