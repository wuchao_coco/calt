package com.calt.plm.basic.core.annoation;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public enum DataType
{

	VARCHAR, NUMBER, DATETIME

}
