package com.calt.plm.basic.core.entity;

import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;

public abstract class GLMaster extends GLObject
{
	@Prop(name = "object_name", type = DataType.VARCHAR, length = 255, index = true)
	private String objectName;
	@Prop(name = "object_code", type = DataType.VARCHAR, length = 30, index = true)
	private String objectCode;

	public String getObjectName()
	{
		return objectName;
	}

	public void setObjectName(String objectName)
	{
		this.objectName = objectName;
	}

	public String getObjectCode()
	{
		return objectCode;
	}

	public void setObjectCode(String objectCode)
	{
		this.objectCode = objectCode;
	}

}
