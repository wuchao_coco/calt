package com.calt.plm.basic.core.annoation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>业务实体注解</p>
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BusinessEntity {

    /**
     * 业务实体的名称，对应表名
     * @return 实体名称
     */
    String name();

    /**
     * 数据源名称
     * @return 数据源名称
     */
    String dataSource() default "dataSource";

}
