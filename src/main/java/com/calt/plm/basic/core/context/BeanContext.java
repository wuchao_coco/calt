package com.calt.plm.basic.core.context;

import java.util.Map;

/**
 * <p>SpringBean上下文</p>
 *
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
public interface BeanContext
{
	/**
	 * 获取bean
	 * @param name 名称
	 * @return bean
	 */
	Object getBean(String name);

	/**
	 * 获取bean
	 * @param clazz 类
	 * @return bean
	 */
	<T> T getBean(Class<T> clazz);

	/**
	 * 获取所有类型的bean
	 * @param type 类
	 * @return 所有的bean
	 */
	<T> Map<String, T> getBeansOfType(Class<T> type);

}
