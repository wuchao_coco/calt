package com.calt.plm.basic.core.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public class ClassUtil extends ClassUtils
{

	public static List<Field> getAllField(Class<?> clazz)
	{
		List<Field> fieldList = new ArrayList<>();

		PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(clazz);
		for (PropertyDescriptor propertyDescriptor : propertyDescriptors)
		{
			String propName = propertyDescriptor.getName();
			// class属性排除，没有set方法的也排除
			if(StringUtils.equalsIgnoreCase("class", propName) || null == propertyDescriptor.getWriteMethod())
			{
				continue;
			}
			fieldList.add(FieldUtils.getField(clazz, propName, true));
		}
		return fieldList;
	}
}
