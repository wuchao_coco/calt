package com.calt.plm.basic.core.command;

import java.util.List;

import com.calt.plm.basic.core.entity.AbstractActionEntity;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public interface EntityCommand
{

	void insert(AbstractActionEntity entity);

	void batchInsert(List<AbstractActionEntity> entities);

	void modify(AbstractActionEntity entity);

	void delete(String id);

	// TODO

}
