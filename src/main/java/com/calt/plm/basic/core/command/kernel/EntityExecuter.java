package com.calt.plm.basic.core.command.kernel;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.calt.plm.basic.core.entity.AbstractActionEntity;
import com.calt.plm.basic.core.entity.AbstractEntity;
import com.calt.plm.basic.core.model.EntityDescribe;
import com.calt.plm.basic.core.model.FieldWrapper;
import com.calt.plm.basic.core.util.PropertyUtil;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class EntityExecuter
{
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private NamedParameterJdbcTemplate jdbcTemplate;

	private EntityDescribe describe;

	public EntityExecuter(EntityDescribe describe)
	{
		this.describe = describe;
		this.jdbcTemplate = new NamedParameterJdbcTemplate(describe.getDs());
	}

	public int insert(AbstractActionEntity entity)
	{
		Map<String, Object> values = new HashMap<>();
		Collection<FieldWrapper> fieldWrappers = this.describe.getColumnMapping().values();
		for (FieldWrapper fieldWrapper : fieldWrappers)
		{
			if(fieldWrapper.isActionField())
			{
				continue;
			}
			values.put(fieldWrapper.getFieldName(), PropertyUtil.getPropValue(entity, fieldWrapper.getFieldName()));
		}
		Map<String, Object> actionProps = entity.getActionProps();
		if(MapUtils.isNotEmpty(actionProps))
		{
			values.putAll(actionProps);
		}
		String sql = this.describe.getInsertSql();
		int update = this.jdbcTemplate.update(sql, values);
		return update;
	}

	public int[] batchInsert(List<AbstractActionEntity> entities)
	{
		Map<String, Object>[] values = new HashMap[]
		{};
		Collection<FieldWrapper> fieldWrappers = this.describe.getColumnMapping().values();

		Map<String, Object> value;
		AbstractActionEntity entity;
		for (int i = 0; i < entities.size(); i++)
		{
			entity = entities.get(i);
			value = new HashMap<>();

			for (FieldWrapper fieldWrapper : fieldWrappers)
			{
				if(fieldWrapper.isActionField())
				{
					continue;
				}
				value.put(fieldWrapper.getFieldName(), PropertyUtil.getPropValue(entity, fieldWrapper.getFieldName()));
			}
			Map<String, Object> actionProps = entity.getActionProps();
			if(MapUtils.isNotEmpty(actionProps))
			{
				value.putAll(actionProps);
			}
			values[i] = value;
		}
		String sql = this.describe.getInsertSql();
		int[] update = this.jdbcTemplate.batchUpdate(sql, values);
		return update;
	}

	public int update(AbstractActionEntity entity)
	{
		Map<String, Object> values = new HashMap<>();
		Collection<FieldWrapper> fieldWrappers = this.describe.getColumnMapping().values();
		for (FieldWrapper fieldWrapper : fieldWrappers)
		{
			if(fieldWrapper.isActionField())
			{
				continue;
			}
			values.put(fieldWrapper.getFieldName(), PropertyUtil.getPropValue(entity, fieldWrapper.getFieldName()));
		}
		Map<String, Object> actionProps = entity.getActionProps();
		if(MapUtils.isNotEmpty(actionProps))
		{
			values.putAll(actionProps);
		}
		String sql = this.describe.getUpdateSql();
		int update = this.jdbcTemplate.update(sql, values);
		return update;
	}

	public void delete(String entityId)
	{
		Map<String, Object> values = new HashMap<>();
		values.put(AbstractEntity.COLUMN_ID, entityId);
		String deleteSql = this.describe.getDeleteByPkSql();
		this.jdbcTemplate.update(deleteSql, values);
	}

}
