package com.calt.plm.basic.core.command.kernel;

import java.util.List;

import com.calt.plm.basic.core.command.EntityCommand;
import com.calt.plm.basic.core.command.kernel.EntityExecuter;
import org.springframework.util.Assert;

import com.calt.plm.basic.core.entity.AbstractActionEntity;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/23
 * @version 1.0
 */
public class EntityCommandImpl implements EntityCommand
{
	private EntityExecuter entityExecuter;

	public EntityCommandImpl(EntityExecuter entityExecuter)
	{
		this.entityExecuter = entityExecuter;
	}

	@Override
	public void insert(AbstractActionEntity entity)
	{
		Assert.notNull(entity, "entity must be not null");
		entityExecuter.insert(entity);
	}

	@Override
	public void batchInsert(List<AbstractActionEntity> entities)
	{
		Assert.notEmpty(entities, "entities must be not empty");
		entityExecuter.batchInsert(entities);
	}

	@Override
	public void modify(AbstractActionEntity entity)
	{
		Assert.notNull(entity, "entity must be not null");
		entityExecuter.update(entity);
	}

	@Override
	public void delete(String id)
	{
		Assert.hasText(id, "id must be has text");
		entityExecuter.delete(id);
	}
}
