package com.calt.plm.basic.core.entity;

import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;

public abstract class GLLink extends AbstractActionEntity
{
	@Prop(name = "role_a_class", type = DataType.VARCHAR, length = 255, index = true)
	private String roleAClass;
	@Prop(name = "role_a_id", type = DataType.VARCHAR, length = 32, index = true)
	private String roleAId;
	@Prop(name = "role_b_class", type = DataType.VARCHAR, length = 255, index = true)
	private String roleBClass;
	@Prop(name = "role_b_id", type = DataType.VARCHAR, length = 32, index = true)
	private String roleBId;

	public String getRoleAClass()
	{
		return roleAClass;
	}

	public void setRoleAClass(String roleAClass)
	{
		this.roleAClass = roleAClass;
	}

	public String getRoleAId()
	{
		return roleAId;
	}

	public void setRoleAId(String roleAId)
	{
		this.roleAId = roleAId;
	}

	public String getRoleBClass()
	{
		return roleBClass;
	}

	public void setRoleBClass(String roleBClass)
	{
		this.roleBClass = roleBClass;
	}

	public String getRoleBId()
	{
		return roleBId;
	}

	public void setRoleBId(String roleBId)
	{
		this.roleBId = roleBId;
	}

}
