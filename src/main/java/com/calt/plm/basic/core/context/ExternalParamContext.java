package com.calt.plm.basic.core.context;

/**
 * <p>外部参数上下文</p>
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
public interface ExternalParamContext
{

	/**
	 * 设置上下文
	 * @param name 参数名称 
	 * @param value 参数值
	 */
	void setParam(String name, Object value);

	/**
	 * 获取参数
	 * @param name 参数名称
	 * @return 参数值
	 */
	Object getParam(String name);

	/**
	 * 获取参数
	 * @param name 参数名称
	 * @param defaultValue 默认值
	 * @return 参数值
	 */
	Object getParam(String name, Object defaultValue);
}
