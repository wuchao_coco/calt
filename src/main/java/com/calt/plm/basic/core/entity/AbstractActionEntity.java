package com.calt.plm.basic.core.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
public abstract class AbstractActionEntity extends AbstractEntity
{
	/**
	 * 所有动作的类型属性
	 */
	protected Map<String, Object> actionProps;

	public AbstractActionEntity()
	{
		this.actionProps = new HashMap<>();
	}

	public void putActionProp(String name, Object prop)
	{
		this.actionProps.put(name, prop);
	}

	/**
	 * 获取 所有动作的类型属性
	 *
	 * @return actionProps 所有动作的类型属性  
	 */
	public Map<String, Object> getActionProps()
	{
		return this.actionProps;
	}

	/**
	 * 设置 所有动作的类型属性
	 *
	 * @param actionProps 所有动作的类型属性  
	 */
	public void setActionProps(Map<String, Object> actionProps)
	{
		this.actionProps = actionProps;
	}

}
