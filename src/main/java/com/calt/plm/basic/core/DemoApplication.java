package com.calt.plm.basic.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
@SpringBootApplication
public class DemoApplication
{

	public static void main(String[] args)
	{
        SpringApplication.run(DemoApplication.class);

		//PartEntity partEntity = new PartEntity(Entity.OPER_TYPE_INSERT);
		//partEntity.setFolderId("folderID");
		//partEntity.setPartCode("partCode");
		//partEntity.setPartPrice(BigDecimal.TEN);
		//partEntity.setProductionTime(new Date());
		//partEntity.setCreateBy("createBy");
		//partEntity.setUpdateBy("updateBy");
		//partEntity.setId("123");
		//partEntity.putActionProp("bizVersion", "bizVersionValue");
		//partEntity.putActionProp("policy_id", "policy_idValue");
		//partEntity.putActionProp("avariable", "avariableValue");
		//partEntity.putActionProp("containerId", "containerIdValue");
		//partEntity.putActionProp("teamId", "teamIdValue");
        //partEntity.putActionProp("bizCurrent", "bizCurrentValue");
        //
        //
        //EntityCommand entityCommand = EntityCommandFactory.get(PartEntity.class);
		//entityCommand.insert(partEntity);

	}

}
