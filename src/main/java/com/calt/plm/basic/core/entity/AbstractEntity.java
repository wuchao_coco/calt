package com.calt.plm.basic.core.entity;

import java.io.Serializable;

import com.calt.plm.basic.core.annoation.DataType;
import com.calt.plm.basic.core.annoation.Prop;

/**
 * <p>抽象实体</p>
 *
 * @author glaway Create on 2022/9/22
 * @version 1.0
 */
public abstract class AbstractEntity extends Entity implements Serializable
{
	private static final long serialVersionUID = 1510807150455399660L;

	public static final String COLUMN_ID = "id";

	/**
	 * ID
	 */
	@Prop(name = COLUMN_ID, type = DataType.VARCHAR, length = 32, index = true)
	protected String id;

	/**
	 * 操作类型
	 */
	protected String operType;

	public AbstractEntity()
	{
	}

	/**
	 * 获取 ID
	 *
	 * @return id ID  
	 */
	public String getId()
	{
		return this.id;
	}

	/**
	 * 设置 ID
	 *
	 * @param id ID  
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * 获取 操作类型
	 *
	 * @return operType 操作类型  
	 */
	public String getOperType()
	{
		return this.operType;
	}

	/**
	 * 设置 操作类型
	 *
	 * @param operType 操作类型  
	 */
	public void setOperType(String operType)
	{
		this.operType = operType;
	}
}
