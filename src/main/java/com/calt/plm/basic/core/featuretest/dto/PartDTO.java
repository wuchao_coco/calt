package com.calt.plm.basic.core.featuretest.dto;

import java.math.BigDecimal;
import java.util.Date;

public class PartDTO
{
    /**
     * 主键
     */
    private String id;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 文件夹主键
     */
    private String folderId;
    /**
     * 部件编码
     */
    private String partCode;
    /**
     * 部件报价
     */
    private BigDecimal partPrice;
    /**
     * 生产时间
     */
    private Date productionTime;
    /**
     * 所属容器主键
     */
    private String containerId;
    /**
     * 容器团队主键
     */
    private String teamId;
    /**
     * 实例唯一标识
     */
    private String bizId;
    /**
     * 生命周期策略主键
     */
    private String policyId;
    /**
     * 生命周期状态
     */
    private String bizCurrent;
    /**
     * -1：删除；1-当前；0-历史
     */
    private String avariable;
    /**
     * 生命周期版本
     */
    private String bizVersion;

    public PartDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getPartCode() {
        return partCode;
    }

    public void setPartCode(String partCode) {
        this.partCode = partCode;
    }

    public BigDecimal getPartPrice() {
        return partPrice;
    }

    public void setPartPrice(BigDecimal partPrice) {
        this.partPrice = partPrice;
    }

    public Date getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(Date productionTime) {
        this.productionTime = productionTime;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getBizCurrent() {
        return bizCurrent;
    }

    public void setBizCurrent(String bizCurrent) {
        this.bizCurrent = bizCurrent;
    }

    public String getAvariable() {
        return avariable;
    }

    public void setAvariable(String avariable) {
        this.avariable = avariable;
    }

    public String getBizVersion() {
        return bizVersion;
    }

    public void setBizVersion(String bizVersion) {
        this.bizVersion = bizVersion;
    }
}
