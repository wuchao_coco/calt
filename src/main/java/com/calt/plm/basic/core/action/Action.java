package com.calt.plm.basic.core.action;

import com.calt.plm.basic.core.context.EntityContext;
import com.calt.plm.basic.core.entity.AbstractActionEntity;

import java.util.List;

/**
 * <p>业务动作</p>
 * @author wuchao Create on 2022/9/20
 * @version 1.0
 */
public interface Action {
    default void save(EntityContext context, List<? extends AbstractActionEntity> target) {
        target.forEach(entity -> {
            context.addInsertEntity(entity);
        });
    }

    default void update(EntityContext context, List<? extends AbstractActionEntity> target) {
        target.forEach(entity -> {
            context.addModifyEntity(entity);
        });
    }

    default void delete(EntityContext context, List<? extends AbstractActionEntity> target) {
        target.forEach(entity -> {
            context.addDeleteEntity(entity);
        });
    }
}
