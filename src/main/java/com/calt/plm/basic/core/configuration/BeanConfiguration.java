package com.calt.plm.basic.core.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.ResourceLoader;

import com.calt.plm.basic.core.container.DataSourceContainer;
import com.calt.plm.basic.core.container.SpringBeanContainer;
import com.calt.plm.basic.core.reader.ActionReader;
import com.calt.plm.basic.core.reader.EntityReader;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * <p>类的简单描述</p>
 *
 * @author wuchao Create on 2022/9/22
 * @version 1.0
 */
@Configuration
public class BeanConfiguration
{
	@Autowired
	private ResourcePatternResolver resourcePatternResolver;

	@Autowired
	private ApplicationContext applicationContext;

	@Bean
	@Order(value = 0)
	public SpringBeanContainer springBeanContainer()
	{
		return new SpringBeanContainer();
	}

	@Bean
	@Order(value = 1)
	public DataSourceContainer dataSourceContainer()
	{
		return new DataSourceContainer();
	}

	@Bean
	@Order(value = 2)
	public ActionReader actionReader()
	{
		ActionReader actionReader = new ActionReader();
		actionReader.init(resourcePatternResolver);
		return actionReader;
	}

	@Bean
	@Order(value = 3)
	public EntityReader entityReader()
	{
		EntityReader entityReader = new EntityReader();
		entityReader.init(applicationContext);
		return entityReader;
	}

}
